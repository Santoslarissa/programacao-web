package ltpweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/cadastroPessoa")
public class CadastroPessoa extends HttpServlet {

	private Map<Email, Pessoa> map = new HashMap<Email, Pessoa>();
	@Override
	public void init() throws ServletException {
		getServletContext().setAttribute("pessoas", map);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Pessoa person = (Pessoa)req.getAttribute("pessoaBean");
		
		Email email = person.getEmail();
		
		if(map.containsKey(email) && req.getParameter("opcao") != null) {
			
			if(req.getParameter("opcao").equals("editar")) {
				map.replace(email, person);
			}
			
			if(req.getParameter("opcao").equals("excluir")) {
				map.remove(email);
			}			
		}else {
			map.put(email, person);
		}		
		
		req.getRequestDispatcher("Historico.jsp").forward(req, resp);

	}
}
