package ltpweb;

import java.util.Calendar;
import java.util.Date;

public class Pessoa {

	private String nome;
	private Date nascimento;
	private Email email;
	
	public Pessoa() {
	}
	
	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}
	
	@Override
	public String toString() {
		return this.nome + " " + this.nascimento + " " + this.email;
	}
	
	public int getIdade() {
		Calendar nasc = Calendar.getInstance();  
		nasc.setTime(nascimento); 
	    Calendar hoje = Calendar.getInstance();  

	    int idade = hoje.get(Calendar.YEAR) - nasc.get(Calendar.YEAR); 

	    if (hoje.get(Calendar.MONTH) < nasc.get(Calendar.MONTH)) {
	      idade--;  
	    } 
	    else 
	    { 
	        if (hoje.get(Calendar.MONTH) == nasc.get(Calendar.MONTH) && hoje.get(Calendar.DAY_OF_MONTH) < nasc.get(Calendar.DAY_OF_MONTH)) {
	            idade--; 
	        }
	    }
	    return idade;
	}
}
