package ltpweb;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/primeiro")
public class Primeiro extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1> Metodo HTTP POST</h1>");
		out.println("<h3> SEJA BEM VINDO(A)! </h3>");
		out.println("<h3> Usuario:" +req.getParameter("Usuario") + "</h3>");
		out.println("<h3> Senha: " + req.getParameter("Senha") + "</h3>");
		out.println("</body>");
		out.println("</html>");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>M�todo HTTP GET</h1>");
		out.println("<h3>Seja bem vindo, "+req.getParameter("usuario") + " " + req.getParameter("senha") + "</h3>");
		out.println("</body>");
		out.println("</html>");
	}
}
